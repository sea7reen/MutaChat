//
//  GeneralNet.h
//  NetworkDemo
//
//  Created by InitialC on 2017/5/4.
//  Copyright © 2016年 InitialC. All rights reserved.
//

#import <Foundation/Foundation.h>


#ifndef DEBUG_URL
#define DEBUG_URL @"http://test.star-fans.com:8000/"
#endif

#ifndef RELEASE_URL
#define RELEASE_URL @"http://test.star-fans.com:8000/"
#endif

#if DEBUG
#define BASE_URL DEBUG_URL
#else
#define BASE_URL RELEASE_URL
#endif


#define  BaseGeneralRequest  @"yanxi://GeneralRequest"


typedef NS_ENUM(NSInteger, NetRequestType)
{
    HttpRequestUrlType_Post,
    HttpRequestUrlType_Get,
    HttpRequestUrlType_Put,
    HttpRequestUrlType_Delete,
    HttpRequestUrlType_Patch,
};


@interface GeneralNet : NSObject
@property(nonatomic, copy)NSString *baseUrl;                     //基础URL
@property(nonatomic, copy)NSString *url;
@property(nonatomic, copy)id parameters;
@property(nonatomic, copy)NSString *type;                        //默认post 总的为6类，post,get ,put,delete,patch,cancel
/** Check CSRF Request Type */
//@property (copy, nonatomic) NSString *specType;
/** Check CSRF Request base url */
//@property (copy, nonatomic) NSString *specBaseURL;

@property(nonatomic, assign)NetRequestType requestType;             //请求类型，默认HttpRequestUrlType_Post
@property(nonatomic, assign)BOOL isCache;                               //是否缓存
@property(nonatomic, assign)BOOL isShowHUD;                             //是否显示成功HUD
@property(nonatomic, assign)BOOL isShowFaildError;                      //是否显示失败HUD
//@property(nonatomic, assign)BOOL isCheckCSRF;                           // 是否检测伪造请求


- (NSDictionary *)dictionaryWithModel;
//清求事件
- (void)request: (void (^)(id result))completion;
- (void)request: (void (^)(id result))completion withFailure: (void (^)(NSError *error))failure;
//- (void)request: (NSString *)csrfURL handler: (void (^)(id result))completion withFailure: (void (^)(NSError *error))failure;
//判断是否请求中
- (BOOL)isRequesting;
- (void)requestStart;
- (void)requestEnd;
/**
 判断是否请求中

 @param url 完整的URL
 @return YES表示请求中，NO表示未请求
 */
+ (BOOL)isRequestingWithURL:(NSString *)url;
//token 过期
- (void)authorizationTokenInvalid;


//请求完成后处理 （因不同后台对code定议不同）
//可用cagetory 来重写 自己处理里面的逻辑
- (id)requestEndLogic:(id)dict;

/**
 设置最基础的URL

 @param url url
 */
+ (void)setBaseURL:(NSString *)url;
//取消请求

+ (void)cancelRequestURL:(NSString *)url;

@end
