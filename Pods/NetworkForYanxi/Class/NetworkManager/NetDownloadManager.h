//
//  NetDownloadManager.h
//  NetworkDemo
//
//  Created by InitialC on 2017/5/22.
//  Copyright © 2016年 InitialC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GeneralNet.h"
typedef void(^DownloadProgress)(NSProgress *progress);

@interface NetDownloadManager : GeneralNet

@property(nonatomic, copy)NSString *savePath;
@property(nonatomic, copy)NSString *fileName;



- (void)request:(DownloadProgress )progress completion:(void (^)(id result))complete;
- (void)request:(DownloadProgress )progress completion:(void (^)(id result))complete withFailure:(void (^)(NSError *error))fail;
@end
