//
//  AliyunManager.h
//  NetworkDemo
//
//  Created by InitialC on 2017/5/22.
//  Copyright © 2017年 InitialC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AliUpdateType){
    AliUpdateTypeTailSize,
    AliUpdateTypeTopicUserSuggest,
    AliUpdateTypeBestStudio,
    AliUpdateTypeOther
};

typedef void(^ProgressBlock)(int64_t progress);
typedef void(^CompletionBlock)(BOOL isSuccess, NSString * _Nullable publicURL, NSString * _Nullable fileName);
@interface AliyunManager : NSObject

@property (assign, nonatomic) AliUpdateType type;

+ (instancetype _Nullable )sharedInstance;

- (void)setupEnvironment;

/**
 注意: 上传的数据必须为文件地址(URL), 或Data

 @param file 上传的数据
 @param folder 上传到文件夹
 @param progressBlock 上传进度
 @param completion 返回是否上传成功, 上传成功后回调的公共地址, 上传的文件名
 */
- (void)uploadObjectAsync:(id _Nullable )file toFolder:(NSString *_Nullable)folder withCount:(NSInteger)count withProgress:(ProgressBlock _Nullable )progressBlock handler:(CompletionBlock _Nullable )completion;

// 同步
- (void)uploadObjectSync:(id _Nullable )file toFolder:(NSString *_Nullable)folder withCount:(NSInteger)count withProgress:(ProgressBlock _Nullable )progressBlock handler:(CompletionBlock _Nullable )completion;

@end

@interface AliyunGeneralObject : NSObject

+ (NSString *_Nullable)getCurrentTimeStamp:(NSString *_Nullable)folder;

+ (NSURL *_Nullable)getFileLocalUrl:(NSString *_Nullable)fileName;

+ (UIImage *_Nullable)resizeImage:(UIImage *_Nullable)oriImage;

@end
