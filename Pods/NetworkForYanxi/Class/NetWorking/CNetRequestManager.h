//
//  CNetRequestManager.h
//  NetworkDemo
//
//  Created by InitialC on 2017/5/27.
//  Copyright © 2016年 InitialC. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kTimeOutInterval 10 // 请求超时的时间
//#define kBaseURL @"http://192.168.1.20:8000"





typedef void (^AFNSuccessBlock)(NSDictionary * _Nonnull dict, BOOL success); // 访问成功block
typedef void (^AFNErrorBlock)(NSError * _Nonnull error); // 访问失败block


@interface CNetRequestManager : NSObject

+ (void)setToken:(nullable NSString *)token withHttpHeader: (nullable NSString *)header;
//+(instancetype _Nonnull)shareInstance;
//网络状态
+ (NSInteger)networkReachabilityStatus;

//是否可连接
+ (BOOL)isReachable;
//设置网络状态改变
+ (void)setReachabilityStatusChangeBlock:(nullable void (^)(NSInteger status))block;

//get 请求
+ (void)CGET:(nullable NSString *)urlString
 parameters:(nullable id)parameters
   progress:(nullable void (^)(NSProgress *_Nonnull downloadProgress))downloadProgress
    success:(nullable AFNSuccessBlock)success
    failure:(nullable AFNErrorBlock)failure;

+ (void)CGET:(nullable NSString *)urlString
 parameters:(nullable id)parameters
    success:(nullable AFNSuccessBlock)success
    failure:(nullable AFNErrorBlock)failure;

//post 请求
+ (void)CPOST:(nullable NSString *)urlString
 parameters:(nullable id)parameters
   progress:(nullable void (^)(NSProgress *_Nonnull downloadProgress))uploadProgress
    success:(nullable AFNSuccessBlock)success
    failure:(nullable AFNErrorBlock)failure;

+ (void)CPOST:(nullable NSString *)urlString
 parameters:(nullable id)parameters
    success:(nullable AFNSuccessBlock)success
    failure:(nullable AFNErrorBlock)failure;

+ (void)CPUT:(nullable NSString *)URLString
    parameters:(nullable id)parameters
    success:(nullable AFNSuccessBlock)success
    failure:(nullable AFNErrorBlock)failure;

+ (void)CPATCH:(nullable NSString *)URLString
parameters:(nullable id)parameters
   success:(nullable AFNSuccessBlock)success
   failure:(nullable AFNErrorBlock)failure;

+ (void)CDELETE:(nullable NSString *)URLString
parameters:(nullable id)parameters
   success:(nullable AFNSuccessBlock)success
   failure:(nullable AFNErrorBlock)failure;


//取消请求
+ (void)cancelRequest:(nullable NSString *)urlString;
//取消当前所有请求
+ (void)cancelAllRequest;
//判断URL是否请求中
+ (BOOL)urlIsRequest:(nullable NSString *)urlString;


#pragma mark -- 下载 （未完成）
//下载
+ (void)downloadWithUrlSring:(nullable NSString *)urlString
                   savePath:(nullable NSString *)savePath
                   progress:(nullable void (^)(NSProgress *_Nonnull downloadProgress))downloadProgress
          completionHandler:(nullable void (^)(NSString *_Nonnull filePath , NSError *_Nonnull error))completion;

//暂停下载
+ (void)pauseDownLoadWithURL:(nullable NSString*)urlString;
//恢复下载
+ (void)reuseDownLoadWithURL:(nullable NSString *)urlString;


#pragma mark  -- 上传 （）



@end
