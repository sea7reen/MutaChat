//
//  YKChatModel.swift
//  MSinger
//
//  Created by Kevin on 2018/3/14.
//  Copyright © 2018年 InitialC. All rights reserved.
//

import UIKit
import ObjectMapper

enum MutaMessageSource {
    case fromChat, fromLittleChicken, formSystem
}

enum MutaMessageType {
    case sendToOther, sendToMe
}

//class MutaChatModel: NSObject, CModelProtocol {

class MutaChatModel: NSObject {
    var messageType: MutaMessageType = .sendToOther
    // 来源
    var chatSource: MutaMessageSource = .fromChat
    // 时间
    var createTime = String()
//    {
//        didSet {
//            createTime = getGeneralTimeFormats(createTime, true)
//        }
//    }
    //只有时间
    var onlyTime = Bool()
    // 是否已读
    var isRead = Int()
    // 图片
    var messageImg = String()
    // 文本
    var messageContent = String()
    // 发送者用户信息
    var sendUserInfo = UserInfoMaster()
    // 接收者用户信息
    var receiveUserInfo = UserInfoMaster()

//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//        chatSource <- map["消息来源"]
//        createTime <- map["发送时间"]
//        isRead <- map["已读"]
//        messageImg <- map["图片信息"]
//        messageContent <- map["消息信息"]
//        sendUserInfo <- map["发送者"]
//        receiveUserInfo <- map["接收者"]
//        messageType <- map["谁发的"]
//    }
}

class UserInfoMaster: NSObject {
    var name = String()
    var header = String()
    var userId = String()
}
