//
//  MutaChatViewController+Gesture.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/22.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit

extension MutaChatViewController {
    //缩放
    func showBigImgView() {
        self.coverView.frame = UIScreen.main.bounds
        self.coverView.backgroundColor = UIColor.black
        let tapBg = UITapGestureRecognizer.init(target: self, action: #selector(tapBgView(tapBgRecognizer:)))
        self.coverView.addGestureRecognizer(tapBg)
        self.coverImgView.image = UIImage.init(named: self.coverImg)
        
        let standardRatio = kZWScreenW/kZWScreenH
        var w: CGFloat = self.coverImgSize.width
        var h: CGFloat = self.coverImgSize.height
        var ratio = CGFloat()
        ratio = self.coverImgSize.width/self.coverImgSize.height
        if ratio > standardRatio {
            w = kZWScreenW
            h = CGFloat(w * self.coverImgSize.height/self.coverImgSize.width)
        }else {
            h = kZWScreenH
            w = h * ratio
        }
        self.coverImgView.frame.size = CGSize(width: w, height: h)
        self.coverImgView.center = self.view.center
        self.coverView.addSubview(self.coverImgView)
        
        self.view.addSubview(self.coverView)
        self.view.bringSubview(toFront: self.coverView)
        self.coverImgView.isUserInteractionEnabled = true
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressClick(gesture:)))
        self.coverImgView.addGestureRecognizer(longPress)
        UIView.animate(withDuration: 0.3) {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    @objc func tapBgView(tapBgRecognizer:UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3, animations: {
            self.coverImgView.frame = CGRect(origin: CGPoint.init(x: self.view.center.x, y: 160) , size: CGSize(width: 0, height: 0))
            tapBgRecognizer.view?.backgroundColor = UIColor.clear
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }) { (finished:Bool) in
            if finished {
                tapBgRecognizer.view?.removeFromSuperview()
            }
        }
    }
    
    //长按手势事件
    @objc func longPressClick(gesture: UILongPressGestureRecognizer) {
        if (gesture.state == .began) {
            let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            let img = UIImageView()
            // TODO: -网络图片转换
            img.image = UIImage.init(named: self.coverImg)
            //             img.setImgVertical(self.coverImg)
            let action = UIAlertAction.init(title: "保存到相册", style: .default) { action in
                UIImageWriteToSavedPhotosAlbum(img.image!, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
            }
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            alert.addAction(action)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        }
    }
    
    //保存图片
    @objc func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error == nil {
            let ac = UIAlertController(title: "保存成功", message: nil, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
}
