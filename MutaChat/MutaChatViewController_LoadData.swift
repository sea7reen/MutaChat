//
//  MutaChatViewController_LoadData.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/23.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit
import MJRefresh

extension MutaChatViewController {
    
    func testModelData() {
        let model = MutaChatModel()
        model.messageContent = "今天有雾霾，天气糟糕透了，明天会怎么样，要看天气预报"
        model.messageImg = ""
        model.messageType = .sendToMe
        model.onlyTime = false
        let user = UserInfoMaster()
        user.name = "QiQi"
        user.header = "header"
        user.userId = "123"
        model.sendUserInfo = user
        model.receiveUserInfo = user
        
        let model1 = MutaChatModel()
        model1.messageContent = "我想吃火锅"
        model1.messageImg = ""
        model1.messageType = .sendToOther
        model1.onlyTime = false
        let user1 = UserInfoMaster()
        user1.name = "KiKi"
        user1.header = "header1"
        user1.userId = "456"
        model1.sendUserInfo = user1
        model1.receiveUserInfo = user1
        
        let model2 = MutaChatModel()
        model2.messageContent = ""
        model2.messageImg = "IMG_0007"
        model2.messageType = .sendToOther
        model2.onlyTime = false
        let user2 = UserInfoMaster()
        user2.name = "KiKi"
        user2.header = "header1"
        user2.userId = "789"
        model2.sendUserInfo = user2
        model2.receiveUserInfo = user2
        
        let model3 = MutaChatModel()
        model3.messageContent = ""
        model3.messageImg = "IMG_0056"
        model3.messageType = .sendToMe
        model3.onlyTime = false
        let user3 = UserInfoMaster()
        user3.name = "QiQi"
        user3.header = "header"
        user3.userId = "000"
        model3.sendUserInfo = user3
        model3.receiveUserInfo = user3
        
        let model4 = MutaChatModel()
        model4.createTime = "2017-01-03"
        model4.onlyTime = true
        
        let model5 = MutaChatModel()
        model5.createTime = "1分钟前"
        model5.onlyTime = true
        
        chatMessageArray = [model4, model2, model, model5, model, model, model1, model3, model2, model1]
    }
    
    func chatRefresh() {
        self.chatTableView.mj_header = refreshHeader(completion: {
            guard self.chatMessageArray.count > 40 else {
                guard self.page == 1 else {
                    self.chatMessageArray = self.chatMessageArray + self.chatMessageArray
                    self.page += 1
                    self.chatTableView.chatData = self.chatMessageArray
                    self.chatTableView.mj_header.endRefreshing()
                    return
                }
                self.page += 1
                self.chatTableView.chatData = self.chatMessageArray
                self.chatTableView.mj_header.endRefreshing()
                return
            }
            self.chatTableView.mj_header.endRefreshing()
        })
    }
}
