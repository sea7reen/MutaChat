//
//  MutaChatViewController_HeightChange.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/26.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit

// MARK: UITextViewDelegate
extension MutaChatViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        content = textView.text
        if textView.text == "" {
            textViewHeight = 36
        }else {
            textViewHeight = self.heightForTextView(textView: textView, fixedWidth: textView.frame.size.width)
        }
        self.textViewHeightChange()
    }
    
    func heightForTextView(textView: UITextView, fixedWidth: CGFloat) -> CGFloat {
        let size = CGSize(width: fixedWidth, height: 16)
        let constraint = textView.sizeThatFits(size)
        return constraint.height
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            if content == "" {
                textView.resignFirstResponder()
                emojiView.removeFromSuperview()
                moreView.removeFromSuperview()
                inputViewIsShown(isShow: false)
                chatTableView.scrollToBottom()
                return false
            }else {
                let model = MutaChatModel()
                model.messageContent = content
                model.messageImg = ""
                model.messageType = .sendToOther
                model.onlyTime = false
                let user = UserInfoMaster()
                user.name = "KiKi"
                user.header = "header1"
                user.userId = "456"
                model.sendUserInfo = user
                model.receiveUserInfo = user
                self.chatMessageArray.append(model)
                self.chatTableView.chatData = self.chatMessageArray
                chatTableView.scrollToBottom()
                content = ""
                textView.text = ""
                
                textViewHeight = 36
                self.textViewHeightChange()
                return false
            }
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let lastStatus = self.chatInputView.chatStatus
        self.chatInputView.chatStatus = .StatusShowKeyboard
        if lastStatus == .StatusShowEmoji {
            self.chatInputView.emojiBtnEmojiImage()
        }else if lastStatus == .StatusShowMore {
            self.chatInputView.addBtnAddImage()
        }
        self.chatInputView.delegate?.chatInputChanged(self.chatInputView, lastStatus!, self.chatInputView.chatStatus!)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        content = textView.text
    }
    
    func textViewHeightChange() {
        UIView.animate(withDuration: 0, animations: {
            self.chatInputView.snp.remakeConstraints { (make) in
                make.width.equalTo(kZWScreenW)
                make.height.equalTo(self.textViewHeight + 16)
                make.bottom.equalToSuperview().offset(self.keyboardHeight)
            }
            self.chatInputView.inputTextView.snp.remakeConstraints { (make) in
                make.left.equalToSuperview().offset(8)
                make.height.equalTo(self.textViewHeight + 5)
                make.right.equalTo(self.chatInputView.emojiBtn.snp.left).offset(-6)
                make.bottom.equalToSuperview().offset(-6)
            }
            
            self.chatTableView.snp.remakeConstraints { (make) in
                make.width.equalTo(kZWScreenW)
                make.bottom.equalTo(self.chatInputView.snp.top)
                make.top.equalToSuperview().offset(64)
            }
        }) { (finish) in
            self.chatTableView.scrollToBottom()
        }
    }
}

extension MutaChatViewController {
    @objc func showView(noti: Notification) {
        let menuVC = (noti.object as? UIMenuController)!
        
        guard !self.view.subviews.contains(self.emojiView) else {
            if menuVC.isMenuVisible {
                menuVC.setMenuVisible(false, animated: true)
            }
            self.emojiView.removeFromSuperview()
            self.inputViewIsShown(isShow: false)
            return
        }
        guard !self.view.subviews.contains(self.moreView) else {
            if menuVC.isMenuVisible {
                menuVC.setMenuVisible(false, animated: true)
            }
            self.moreView.removeFromSuperview()
            self.inputViewIsShown(isShow: false)
            return
        }
        self.chatInputView.inputTextView.resignFirstResponder()
        self.inputViewIsShown(isShow: false)
        return
    }
    
    func keyboardShown(hight: CGFloat) {
        keyboardHeight = hight
        UIView.animate(withDuration: showKeyBoardDuration) {
            self.chatInputView.snp.remakeConstraints { (make) in
                make.width.equalTo(kZWScreenW)
                make.height.equalTo(52)
                make.bottom.equalToSuperview().offset(hight)
            }
        }
    }
    
    func inputViewIsShown(isShow: Bool) {
        var h = CGFloat()
        h = (isShow ? -215 : 0)
        
        UIView.animate(withDuration: hideKeyBoardDuration, animations: {
            self.chatInputView.snp.remakeConstraints { (make) in
                make.width.equalTo(kZWScreenW)
                make.height.equalTo(52)
                make.bottom.equalToSuperview().offset(h)
            }
            self.chatInputView.inputTextView.snp.remakeConstraints { (make) in
                make.left.equalToSuperview().offset(8)
                make.height.equalTo(40)
                make.right.equalTo(self.chatInputView.emojiBtn.snp.left).offset(-6)
                make.bottom.equalToSuperview().offset(-6)
            }
            self.chatTableView.snp.remakeConstraints { (make) in
                make.width.equalTo(kZWScreenW)
                make.bottom.equalTo(self.chatInputView.snp.top)
                make.top.equalToSuperview().offset(64)
            }
        }) { (finish) in
            self.chatTableView.scrollToBottom()
        }
    }
    
    @objc func keyboardFrameWillShow(_ notification: NSNotification) {
        let keyboardSize = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
        let hight = keyboardSize.cgRectValue.height
        // 得到键盘弹出所需时间
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        showKeyBoardDuration = duration.doubleValue
        keyboardShown(hight: -hight)
        UIMenuController.shared.menuItems = nil
    }
    
    @objc func keyboardFrameDidShow(_ notification: NSNotification) {
        chatTableView.scrollToBottom()
    }
    
    @objc func keyboardFrameWillHide(_ notification: NSNotification) {
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        hideKeyBoardDuration = duration.doubleValue
        inputViewIsShown(isShow: false)
    }
    
    @objc func keyboardFrameDidHide(_ notification: NSNotification) {
        chatTableView.scrollToBottom()
    }
}
