//
//  MutaMoreItemView.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/19.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit

class MutaMoreItemView: UIView {
    
    var image = String() {
        didSet {
            self.button.setImage(UIImage.init(named: image), for: .normal)
        }
    }
    
    var name = String() {
        didSet {
            self.title.text = name
        }
    }

    var button: UIButton = {
        let btn = UIButton()
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 4
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.gray.cgColor
        return btn
        }()
    
    var title: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .center
        label.textColor = UIColor.darkGray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(button)
        self.addSubview(title)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var frame: CGRect {
        didSet {
            super.frame = frame
            let w = 59 as CGFloat
            self.button.frame = CGRect(x: (self.frame.size.width - w)/2, y: 0, width: w, height: w)
            self.title.frame =  CGRect(x: -5, y: self.button.frame.size.height + 5, width: self.frame.size.width + 10, height: 15)
        }
    }
    
    override var tag: Int {
        didSet {
            super.tag = tag
            self.button.tag = tag
        }
    }
    
    func addTarget(target: Any?, action: Selector, for controlEvents: UIControlEvents) {
        self.button.addTarget(target, action: action, for: controlEvents)
    }
    
    static func createMoreItemWithTitle(name: NSString, withImage image: NSString) -> MutaMoreItemView {
        let item = MutaMoreItemView()
        item.name = name as String
        item.image = image as String
        return item
    }
}
