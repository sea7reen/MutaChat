//
//  MutaChatInputView.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/14.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit
import SnapKit

enum MutaChatInPutStatus {
    case StatusNone,
    StatusShowEmoji,
    StatusShowMore,
    StatusShowKeyboard
}

protocol MutaChatInputDelegate {
    func chatInputChanged(_ inputView: MutaChatInputView, _ fromStatus: MutaChatInPutStatus, _ toStatus: MutaChatInPutStatus)
}

class MutaChatInputView: UIView {
    var delegate: MutaChatInputDelegate?
    var addSegue: ValueClosure?
    var emojiSegue: ValueClosure?
    var chatStatus: MutaChatInPutStatus?
    
    var inputTextView: MutaTextView = {
        let textView = MutaTextView()
        textView.layer.cornerRadius = 6
        textView.layer.borderColor = UIColor.gray.cgColor
        textView.layer.borderWidth = 1
        textView.layer.masksToBounds = true
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.tintColor = UIColor.black
        textView.contentInset = UIEdgeInsetsMake(2, 2, 2, 2)
        return textView
    }()
    
    var emojiBtn: UIButton = {
        let button = UIButton()
        button.setImage(UIImage.init(named: "ToolViewEmotion"), for: UIControlState.normal)
        button.setImage(UIImage.init(named: "ToolViewEmotionHL"), for: UIControlState.highlighted)
        button.addTarget(self, action: #selector(touchEmojiBtn(sender:)), for: UIControlEvents.touchUpInside)
        return button
    }()
    
    var addBtn: UIButton = {
        let button = UIButton()
        button.setImage(UIImage.init(named: "TypeSelectorBtn_Black"), for: .normal)
        button.setImage(UIImage.init(named: "TypeSelectorBtnHL_Black"), for: .highlighted)
        button.addTarget(self, action: #selector(touchAddBtn(sender:)), for: .touchUpInside)
        return button
    }()
    
    @objc func touchAddBtn(sender: UIButton) {
        let lastStatus = self.chatStatus
        if lastStatus == .StatusShowMore {
            chatStatus = .StatusShowKeyboard
            self.addBtnAddImage()
            self.inputTextView.becomeFirstResponder()
            self.delegate?.chatInputChanged(self, lastStatus!, chatStatus!)
        }else {
            chatStatus = .StatusShowMore
            self.addBtnKeyboardImage()
            if lastStatus == .StatusShowEmoji {
                self.emojiBtnEmojiImage()
            }else if lastStatus == .StatusShowKeyboard {
                self.inputTextView.resignFirstResponder()
            }
            self.delegate?.chatInputChanged(self, lastStatus!, chatStatus!)
        }
    }

    @objc func touchEmojiBtn(sender: UIButton) {
        let lastStatus = self.chatStatus
        if lastStatus == .StatusShowEmoji {
            chatStatus = .StatusShowKeyboard
            self.emojiBtnEmojiImage()
            self.inputTextView.becomeFirstResponder()
            self.delegate?.chatInputChanged(self, lastStatus!, chatStatus!)

        }else {
            chatStatus = .StatusShowEmoji
            self.emojiBtnKeyboardImage()
            if lastStatus == .StatusShowMore {
                self.addBtnAddImage()
            }else if lastStatus == .StatusShowKeyboard {
                self.inputTextView.resignFirstResponder()
            }
            self.delegate?.chatInputChanged(self, lastStatus!, chatStatus!)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addView(){
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
        
        self.addSubview(inputTextView)
        self.addSubview(emojiBtn)
        self.addSubview(addBtn)
        
        addBtn.snp.makeConstraints { (make) in
            make.width.height.equalTo(36)
            make.right.equalToSuperview().offset(-10)
            make.bottom.equalToSuperview().offset(-8)
        }
        emojiBtn.snp.makeConstraints { (make) in
            make.width.height.equalTo(36)
            make.right.equalTo(addBtn.snp.left).offset(-2)
            make.bottom.equalToSuperview().offset(-8)
        }
        inputTextView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(8)
            make.height.equalTo(40)
            make.right.equalTo(emojiBtn.snp.left).offset(-6)
            make.bottom.equalToSuperview().offset(-6)
        }
    }
    
    func addBtnAddImage() {
        addBtn.setImage(UIImage.init(named: "TypeSelectorBtn_Black"), for: UIControlState.normal)
        addBtn.setImage(UIImage.init(named: "TypeSelectorBtnHL_Black"), for: UIControlState.highlighted)
    }
    
    func addBtnKeyboardImage() {
        addBtn.setImage(UIImage.init(named: "ToolViewKeyboard"), for: UIControlState.normal)
        addBtn.setImage(UIImage.init(named: "ToolViewKeyboardHL"), for: UIControlState.highlighted)
    }
    
    func emojiBtnEmojiImage() {
        emojiBtn.setImage(UIImage.init(named: "ToolViewEmotion"), for: UIControlState.normal)
        emojiBtn.setImage(UIImage.init(named: "ToolViewEmotionHL"), for: UIControlState.highlighted)
    }
    
    func emojiBtnKeyboardImage() {
        emojiBtn.setImage(UIImage.init(named: "ToolViewKeyboard"), for: UIControlState.normal)
        emojiBtn.setImage(UIImage.init(named: "ToolViewKeyboardHL"), for: UIControlState.highlighted)
    }
    
}
