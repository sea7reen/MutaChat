//
//  YKChatTableView.swift
//  MSinger
//
//  Created by Kevin on 2018/3/14.
//  Copyright © 2018年 InitialC. All rights reserved.
//

import UIKit

private let TimeHeader = "MutaChatDataTimeHeader"
private let DataCellID = "MutaChatDataCell"
private let TimeCellID = "MutaChatTimeCell"

class MutaChatTableView: UITableView, UITableViewDelegate, UITableViewDataSource {

    var toVCSegue: ValueClosure?
    
    var deleteSegue: ValueClosure?
    
    var showImgSegue: ValueSegue?
    
    var selectRow = Int()
    
    var chatData: [MutaChatModel]? {
        didSet {
            guard let data = chatData, data.count > 0 else {
                return
            }
            self.reloadData()
        }
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        self.register(MutaChatDataCell.self, forCellReuseIdentifier: DataCellID)
        self.register(MutaChatTimeCell.self, forCellReuseIdentifier: TimeCellID)
        self.dataSource = self
        self.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataCell = tableView.dequeueReusableCell(withIdentifier: DataCellID) as! MutaChatDataCell
        let timeCell = tableView.dequeueReusableCell(withIdentifier: TimeCellID) as! MutaChatTimeCell
        if chatData?.count == 0 {
            return dataCell
        }
        //缓存cell高度
        dataCell.useCellFrameCache(with: indexPath, tableView: tableView)
        timeCell.useCellFrameCache(with: indexPath, tableView: tableView)
        
        let model = self.chatData![indexPath.item]
        if model.onlyTime {
            timeCell.timeLalel.text = model.createTime
            return timeCell
        }else {
            dataCell.model = self.chatData![indexPath.item]
            dataCell.headerSegue = {id in
                self.toVCSegue!(id)
            }
            dataCell.sendStateSegue = {sender in
                //                self.reloadData()
            }
            dataCell.imgTapSegue = {img, size in
                self.showImgSegue!(img, size)
            }
//            dataCell.contentLalel.deleteSegue = {
//                self.deleteSegue!(indexPath.item)
//                print("点击了文本删除")
//            }
            dataCell.contentImg.deleteSegue = {
                self.deleteSegue!(indexPath.item)
                print("点击了图片删除")
            }
            return dataCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if chatData?.count == 0 {
            return 0.1
        }
        let model = self.chatData![indexPath.item]
        if model.onlyTime {
            return 22
        }else {
            let hight = tableView.cellHeight(for: indexPath, cellContentViewWidth: kZWScreenW, tableView: tableView)
            return hight
        }
    }
    
    func scrollToBottom() {
        guard ((chatData?.count) != nil) else {
            return
        }
        self.scrollToRow(at: IndexPath.init(row: (chatData?.count)! - 1, section: 0), at: .bottom, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        // 隐藏菜单
        let menuVC = UIMenuController.shared
        menuVC.setMenuVisible(false, animated: true)
    }
}
