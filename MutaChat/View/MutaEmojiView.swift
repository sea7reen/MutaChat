//
//  MutaEmojiView.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/16.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit

class MutaEmojiView: UIView, UIScrollViewDelegate {
    var moreSegue: ValueClosure?
    
    var moreScrollView: UIScrollView = {
        let view = UIScrollView()
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.isPagingEnabled = true
        view.scrollsToTop = false
        return view
    }()
    
    var pageControl: UIPageControl = {
        let control = UIPageControl()
        control.currentPageIndicatorTintColor = UIColor.darkGray
        control.pageIndicatorTintColor = UIColor.lightGray
        control.addTarget(self, action: #selector(clickControl(sender:)), for: .valueChanged)
        return control
    }()
    
    var topLine: UIView = {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: kZWScreenW, height: 1))
        view.backgroundColor = UIColor.darkGray
        return view
    }()
    

    @objc func didSelectedItem(itemView: MutaMoreItemView) {
        self.moreSegue!(itemView.tag)
    }
    
    @objc func clickControl(sender: UIPageControl) {
        let rect = CGRect(x:CGFloat(sender.currentPage) * kZWScreenW, y: 0, width: kZWScreenW, height: self.moreScrollView.height)
        self.moreScrollView.scrollRectToVisible(rect, animated: true)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.moreScrollView)
        self.addSubview(self.pageControl)
        self.addSubview(self.topLine)
        self.moreScrollView.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var frame: CGRect {
        didSet {
            super.frame = frame
            self.moreScrollView.frame = CGRect(x: 0, y: 1, width: frame.size.width, height: frame.size.height - 18 - 36)
            self.pageControl.frame = CGRect(x: 0, y: self.moreScrollView.frame.size.height + 3 , width: frame.size.width, height: 8)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / self.frame.size.width
        self.pageControl.currentPage = Int(page)
    }
}
