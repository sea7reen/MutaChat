//
//  MutaChatTimeCell.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/15.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit
import SnapKit

class MutaChatTimeCell: UITableViewCell {
    lazy var timeLalel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.darkGray
        label.numberOfLines = 0
        label.layer.cornerRadius = 2
        label.layer.masksToBounds = true
        label.textAlignment = .center
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.timeLalel.frame = CGRect(x: (kZWScreenW - 60)/2, y: 6, width: 60, height: 14)
    }
    
    func setupView() {
        self.selectionStyle = .none
        self.contentView.addSubview(self.timeLalel)
    }
}
