//
//  MutaMoreView.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/16.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit

class MutaMoreView: UIView, UIScrollViewDelegate {

    var moreSegue: ValueClosure?
    
    var moreScrollView: UIScrollView = {
        let view = UIScrollView()
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.isPagingEnabled = true
        view.scrollsToTop = false
        return view
    }()
    
    var pageControl: UIPageControl = {
        let control = UIPageControl()
        control.currentPageIndicatorTintColor = UIColor.darkGray
        control.pageIndicatorTintColor = UIColor.lightGray
        control.addTarget(self, action: #selector(clickControl(sender:)), for: .valueChanged)
        return control
        }()
    
    var topLine: UIView = {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: kZWScreenW, height: 1))
        view.backgroundColor = UIColor.darkGray
        return view
        }()
    
    var itemsAry = [MutaMoreItemView]() {
        didSet {
            self.pageControl.numberOfPages = itemsAry.count / 8 + 1
            self.moreScrollView.contentSize = CGSize(width: kZWScreenW * CGFloat(itemsAry.count / 8 + 1), height: moreScrollView.frame.size.height)
            
            let w = self.frame.size.width * 20 / 21 / 4 * 0.8
            let space: CGFloat = w / 4
            let h = (frame.size.height - 20 - space * 2) / 2
            var x = space, y = space
            var i = Int(), page = Int()
            i = 0
            page = 0
            for item: MutaMoreItemView in itemsAry {
                self.moreScrollView.addSubview(item)
                item.frame = CGRect(x: x, y: y, width: w, height: h)
                item.tag = i
                item.addTarget(target: self, action: #selector(didSelectedItem(itemView:)), for: .touchUpInside)
                i += 1
                page = i%8 == 0 ? page + 1: page
                x = (i % 4 != 0 ? x + w : CGFloat(page)*self.frame.size.width) + space
                y = i%8 < 4 ? space: (h + space * 1.5)
            }
        }
    }
    
    @objc func didSelectedItem(itemView: MutaMoreItemView) {
        self.moreSegue!(itemView.tag)
    }
    
    @objc func clickControl(sender: UIPageControl) {
        let rect = CGRect(x:CGFloat(sender.currentPage) * kZWScreenW, y: 0, width: kZWScreenW, height: self.moreScrollView.height)
        self.moreScrollView.scrollRectToVisible(rect, animated: true)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        self.addSubview(self.pageControl)

        self.addSubview(self.moreScrollView)
//        self.addSubview(self.topLine)
        self.moreScrollView.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var frame: CGRect {
        didSet {
            super.frame = frame
            self.moreScrollView.frame = CGRect(x: 0, y: 1, width: frame.size.width, height: frame.size.height - 18)
            self.pageControl.frame = CGRect(x: 0, y: frame.size.height - 18 , width: frame.size.width, height: 8)
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / self.frame.size.width
        self.pageControl.currentPage = Int(page)
    }
}
