//
//  MenuUIImageView.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/23.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit

class MenuUIImageView: UIImageView {
    var deleteSegue: ClickSegue?
    
    var menuVC = UIMenuController.shared

    var pressClosure: ((_ gesture: UILongPressGestureRecognizer) -> ())? // 长按事件

    var isOpenLongGesture: Bool = true {
        didSet {
            self.addLongPressGesture()
        }
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if isOpenLongGesture {
            self.addLongPressGesture()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        if isOpenLongGesture {
            self.addLongPressGesture()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(deleteImg(_:)) {
            return true
        }else{
            return false
        }
    }
    
    @objc func deleteImg(_ sender: Any?) {
        self.deleteSegue!()
    }
}

extension MenuUIImageView {
    func addLongPressGesture() {
        self.isUserInteractionEnabled = true
        let longPressGesture = UILongPressGestureRecognizer.init(target: self, action: #selector(longPressAction(gesture:)))
        self.addGestureRecognizer(longPressGesture)
    }
    
    @objc func longPressAction(gesture: UILongPressGestureRecognizer){
        if gesture.state == .began {
            self.becomeFirstResponder()
            let deleteItem = UIMenuItem(title: "删除", action: #selector(deleteImg(_:)))
            menuVC.menuItems = [deleteItem]
            if menuVC.isMenuVisible {
                return
            }
            menuVC.setTargetRect(bounds, in: self)
            menuVC.setMenuVisible(true, animated: true)
            NotificationCenter.default.post(name: NSNotification.Name.init("图片长按手势启动"), object: menuVC)
        }
        if gesture.state == .ended{

        }
    }
}
