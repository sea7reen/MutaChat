//
//  Tool.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/14.
//  Copyright © 2018年 kevin. All rights reserved.
//

// 屏幕宽高
let kZWScreenW = UIScreen.main.bounds.size.width
let kZWScreenH = UIScreen.main.bounds.size.height

let kMaxContainerWidth = CGFloat(220)
let kMaxLabelWidth = CGFloat(kMaxContainerWidth - 40)
let kMaxChatImageViewWidth = CGFloat(140)
let kMaxChatImageViewHeight = CGFloat(226)


import UIKit
import ObjectMapper
import Kingfisher


typealias CModelProtocol = ObjectMapper.Mappable
typealias CMMappable = ObjectMapper.ImmutableMappable
typealias CMapper = ObjectMapper.Mapper

typealias ValueClosure = (Any)->()
typealias ValueSegue = (String, Any)->()
typealias RefreshClosure = ()->()
typealias ClickSegue = ()->()


/// 个性化时间格式
func getGeneralTimeFormats(_ timeStr : String, _ isSpec : Bool) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    if let date = formatter.date(from: timeStr) {
        let timeStr = isSpec ? NSDate.timeInfo(withSpecDate: date)! : NSDate.timeInfo(with: date)!
        return timeStr
    } else {
        return ""
    }
}

extension UIImageView {
    func setImgVertical(_ img: String) {
        setImg(img, placeHolder: "")
    }
    
    private func setImg(_ img: String, placeHolder: String) {
        self.kf.setImage(with: URL(string: img), placeholder: UIImage(named: placeHolder), options: nil, progressBlock: nil, completionHandler: nil)
    }
}
