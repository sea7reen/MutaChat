//
//  MutaTextView.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/26.
//  Copyright © 2018年 kevin. All rights reserved.
//

import UIKit

class MutaTextView: UITextView {
    
    weak var inputNextResponder: UIResponder?
    
    override var next: UIResponder? {
        if let responder = inputNextResponder {
            
            return responder }
        return super.next
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if inputNextResponder != nil { return false }
        return super.canPerformAction(action, withSender: sender)
    }
}
