//
//  RefreshManager.swift
//  MutaChat
//
//  Created by Kevin on 2018/3/22.
//  Copyright © 2018年 kevin. All rights reserved.
//

import Foundation
import MJRefresh

extension UIViewController {
    func refreshHeader(completion: @escaping RefreshClosure) -> MJRefreshNormalHeader {
        let header = MJRefreshNormalHeader {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                completion()
            })
        }
        header?.stateLabel.text = "加载中"
        header?.lastUpdatedTimeLabel.isHidden = true
        header?.setTitle("", for: .pulling)
        header?.setTitle("下拉加载", for: .idle)
        header?.setTitle("加载中", for: .refreshing)
        return header!
    }
    func refreshFooter(completion: @escaping RefreshClosure) -> MJRefreshAutoNormalFooter {
        let footer = MJRefreshAutoNormalFooter {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                completion()
            })
        }
        return footer!
    }
}
